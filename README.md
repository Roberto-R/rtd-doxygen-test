# Example of C++ docs hosted on ReadTheDocs #

[![Documentation Status](https://readthedocs.org/projects/rtd-doxygen-test/badge/?version=latest)](https://rtd-doxygen-test.readthedocs.io/en/latest/?badge=latest)

Doxygen output can be parsed by Sphinx, through the "Breathe" extension.

The documentation can be found at: <https://rtd-doxygen-test.readthedocs.io/en/latest/>

## Build ##

The code can be built with:

```shell
mkdir build && cd build
cmake ..
make
```

## Docs ##

Build the documentation with:

```shell
cd docs
make html
```

This will trigger Sphinx to build the documentation. Sphinx in turn will call Doxygen.
There is no need to run Doxygen directly. It is however still possible to do so:

```shell
cd docs
doxygen Doxyfile.in
```

Either way, the output can be found in `docs/_build/html/index.html`.

## ReadTheDocs.org ##

For this to work with RTD no extra steps were needed. It's important that a 
`requirements.txt` contains the `sphinx-breathe` package. The project settings
were left at the default.