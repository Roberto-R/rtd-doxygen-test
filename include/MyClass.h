//
// Created by robert on 21-01-21.
//

#ifndef INCLUDE_MYCLASS_H_
#define INCLUDE_MYCLASS_H_


/**
 * This is my class!
 *
 * A longer description.
 */
class MyClass {
public:

    /**
     * Some silly public variable
     */
    int x;

    /**
     * My constructor
     */
    MyClass();
};


#endif //INCLUDE_MYCLASS_H_
